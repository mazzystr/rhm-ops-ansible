#!/usr/bin/env python
DOCUMENTATION = '''
---
module: rhmap_generate_inventory_hosts
short_description: Generate the inventory/hosts file from the fh-aws-environment-registry.json and env-type json files
description:
    - Generate the inventory/hosts file from the fh-aws-environment-registry.json and env-type json files
version_added: "2.3"
options:
    env_types_path:
        description:
            - This is the path to the directory containing the env-types JSON files
        required: true
    aws_registry_path:
        description:
            - This is the path to the fh-aws-environment-registry JSON file
        required: true
    inventory_path:
        description:
            - This is the path to the inventory/hosts file
        required: true
author: "David Kirwan (dkirwan@redhat.com)"
'''

EXAMPLES = '''
# Generate the inventory/hosts file for the RHMAP SaaS environments
- rhmap_generate_inventory_hosts:
    env_types_path: /path/to/env-types/
    aws_registry_path: /path/to/fh-aws-environment-registry.json
    inventory_path: /path/to/the/inventory/hosts
'''

RETURN = '''
hosts:
    description: The RHMAP SaaS inventory
    type: dictionary
'''

try:
    import time
    import json
    import yaml
    import os
    import sys
    from ansible.module_utils.basic import AnsibleModule

except ImportError:
    print("Import error")
    

def run_module():
    # define the available arguments/parameters that a user can pass to
    # the module
    module_args = dict(
        env_types_path=dict(type='str', required=True),
        aws_registry_path=dict(type='str', required=True),
        inventory_path=dict(type='str', required=True)
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # change is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        hosts={}
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        return result

    try:
        # manipulate or modify the state as needed (this is going to be the
        # part where your module will do what it needs to do)

        env_types_path = module.params['env_types_path']
        aws_registry_path = module.params['aws_registry_path']
        inventory_path = module.params['inventory_path']


        # Read in the contents of the fh-aws-environment-registry.json file
        with open(aws_registry_path) as data_file:
            aws_registry = json.load(data_file)

        # Read in the JSON files in the env-types 
        with open( os.path.join( env_types_path, 'rhm-env-types-core.json') ) as data_file:
            env_types_core = json.load(data_file) 
        with open( os.path.join( env_types_path, 'rhm-env-types-farm.json') ) as data_file:
            env_types_farm = json.load(data_file) 
        with open( os.path.join( env_types_path, 'rhm-env-types-mbaas.json') ) as data_file:
            env_types_mbaas = json.load(data_file)
        with open( os.path.join( env_types_path, 'rhm-env-types-noc.json') ) as data_file:
            env_types_noc = json.load(data_file)

        env_types = {
            'noc':env_types_noc["noc"],
            'core':env_types_core["core"],
            'farm':env_types_farm["farm"],
            'mbaas':env_types_mbaas["mbaas"]
        }

        result['hosts'] = {
            'aws_registry':aws_registry,
            'env_types':env_types
        }   
       
        inventory = {
            'jarvis': ["jarvis"],
            'trigger': ["trigger"]
        }

        for key, val in result['hosts']['aws_registry'].iteritems():
            site_name = key
            site_type = val['type']
            site_size = val['size']
            site_connection = val['conn']
            site_servers = result['hosts']['env_types'][site_type][site_size]

            inventory[site_name] = []

            for k, v in site_servers.iteritems():
                if k != "elbs":
                    if k == "gw" and site_connection == "nat":
                        pass # Do not add entries for gateway instances when the connection type is nat
                    else:
                        instance_type = k
                        qty = int(v["qty"])
                        for num in range(1, qty + 1):
                            inventory[site_name].append("%s-%s%d" % (site_name, instance_type, num))

        f = open(inventory_path, "w")
        for key, val in inventory.iteritems():
            f.write("[%s]\n" % key)

            for v in val:
                f.write("%s.feedhenry.net \t ansible_connection=ssh \t ansible_user=hadmin \t ansible_python_interpreter=/usr/bin/python\n" % v)
            f.write("\n")

        f.write("[rhmap_saas_infrastructure:children]\n")
        for key in inventory.keys():
            f.write(key)
            f.write("\n")

        f.close


        # use whatever logic you need to determine whether or not this module
        # made any modifications to your target

        #result['changed'] = True
    except Exception as e:
        # during the execution of the module, if there is an exception or a
        # conditional state that effectively causes a failure, run
        # AnsibleModule.fail_json() to pass in the message and the result
        module.fail_json(msg=e, **result)

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)

def main():
    run_module()

if __name__ == '__main__':
    main()
