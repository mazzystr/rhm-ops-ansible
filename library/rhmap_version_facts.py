#!/usr/bin/env python
DOCUMENTATION = '''
---
module: rhmap_version_facts
short_description: Gather facts about the versions of RHMAP Components
description:
    - Gather facts about the versions of RHMAP Components
version_added: "2.2"
options:
    version_path:
        description:
            - This is the path of the JSON component versions file
        required: true
author: "David Kirwan (dkirwan@redhat.com)"
'''

EXAMPLES = '''
# Gather facts about the RHMAP component version
- rhmap_version_facts:
    version_path: /path/to/COMPONENT_VERSIONS.json
'''

RETURN = '''
components:
    description: The RHMAP component versions in this release
    type: dictionary
'''

try:
    import time
    import json
    import yaml
    from ansible.module_utils.basic import AnsibleModule

except ImportError:
    print("Import error")
    

def run_module():
    # define the available arguments/parameters that a user can pass to
    # the module
    module_args = dict(
        version_path=dict(type='str', required=True)
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # change is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        components=[]
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        return result

    try:
        # manipulate or modify the state as needed (this is going to be the
        # part where your module will do what it needs to do)

        # - read in the json data from the path
        # - return it back to the playbook which called it
        path = module.params['version_path']
        # Read in the saved follower list
        with open(path) as data_file:
            rhmap_versions = json.load(data_file)
        result['components'] = rhmap_versions["components"]

        # use whatever logic you need to determine whether or not this module
        # made any modifications to your target
        result['changed'] = False

    except Exception as e:
        # during the execution of the module, if there is an exception or a
        # conditional state that effectively causes a failure, run
        # AnsibleModule.fail_json() to pass in the message and the result
        module.fail_json(msg=e, **result)

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)

def main():
    run_module()

if __name__ == '__main__':
    main()
