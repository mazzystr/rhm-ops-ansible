#!/usr/bin/env python

from ansible.errors import AnsibleError
from os.path import expanduser


try:
    from ansible.plugins.lookup import LookupBase
except ImportError:
    class LookupBase(object):
        def __init__(self, basedir=None, runner=None, **kwargs):
            self.runner = runner
            self.basedir = self.runner.basedir

            def get_basedir(self, variables):
                return self.basedir


class LookupModule(LookupBase):
    def run(self, terms, variables=None, **kwargs):
        try:
            data = []
            try:
                lines = open(expanduser("~") + '/.aws/credentials')
            except Exception as e:
                raise AnsibleError("Cannot open credentials file")
            for line in lines:
                line = line.strip()
                if line.startswith('#') or \
                        line.startswith(';') or \
                        len(line) == 0:
                    continue
                if line.startswith('aws_access_key_id'):
                    aws_access_key_id = line[20:]
                if line.startswith('aws_secret_access_key'):
                    aws_secret_access_key = line[24:]
                try:
                    if aws_access_key_id:
                        if aws_secret_access_key:
                            data.append(
                                [
                                    {
                                        'aws_access_key_id':
                                        aws_access_key_id,
                                        'aws_secret_access_key':
                                        aws_secret_access_key
                                    }
                                ]
                            )
                            return data
                except:
                    pass
            return data
        except Exception as e:
            raise AnsibleError("Error")
