#!/usr/bin/env python

from ansible import errors
import boto3


try:
    # ansible-2.0
    from ansible.plugins.lookup import LookupBase
except ImportError:
    # ansible-1.9.x
    class LookupBase(object):
        def __init__(self, basedir=None, runner=None, **kwargs):
            self.runner = runner
            self.basedir = self.runner.basedir

            def get_basedir(self, variables):
                return self.basedir


class LookupModule(LookupBase):
    def __init__(self, basedir=None, **kwargs):
        self.basedir = basedir

    def run(self, args, inject=None, **kwargs):
        try:
            for a in list(args):
                if 'aws_access_key' in a:
                    aws_access_key = a['aws_access_key']
                if 'aws_secret_key' in a:
                    aws_secret_key = a['aws_secret_key']
                if 'security_token' in a:
                    security_token = a['security_token']
                if 'region' in a:
                    region = a['region']
        except Exception as e:
            raise errors.AnsibleError("%s" % (e))

        try:
            zones = []
            ec2 = boto3.client(
                'ec2',
                region,
                aws_access_key_id=aws_access_key,
                aws_secret_access_key=aws_secret_key,
                aws_session_token=security_token)
            response = ec2.describe_availability_zones()
            for k in response['AvailabilityZones']:
                zones.append(k['ZoneName'])
            return(zones)
        except Exception as e:
            raise errors.AnsibleError("%s" % (e))
