#!/usr/bin/env python

from ansible.errors import AnsibleError
from os.path import expanduser


try:
    from ansible.plugins.lookup import LookupBase
except ImportError:
    class LookupBase(object):
        def __init__(self, basedir=None, runner=None, **kwargs):
            self.runner = runner
            self.basedir = self.runner.basedir

            def get_basedir(self, variables):
                return self.basedir


class LookupModule(LookupBase):
    def run(self, terms, variables=None, **kwargs):
        try:
            data = []
            try:
                lines = open(expanduser("~") + '/.aws/credentials')
            except Exception as e:
                raise AnsibleError("Cannot open credentials file")
            for line in lines:
                line = line.strip()
                if line.startswith('#') \
                        or line.startswith(';') \
                        or len(line) == 0:
                    continue
                if line.startswith('[profile '):
                    profile = line[1:-1].replace('profile ', '')
                if line.startswith('[default]'):
                    profile = line[1:-1].replace('profile ', '')
                try:
                    if profile is not None:
                        if ('default' in profile):
                            if 'source_profile' in line:
                                profile = line[17:]
                        if ('role_arn' in line):
                            kv = line.split()
                            role_arn = kv[2:][0]
                        try:
                            if 'default' not in profile and profile != '':
                                if 'arn:aws:iam' in role_arn:
                                    data.append(
                                        [
                                            {
                                                'profile':
                                                profile,
                                                'role_arn':
                                                role_arn
                                            }
                                        ]
                                    )
                                    profile = ''
                        except:
                            pass
                except:
                    pass
            return data
        except Exception as e:
            raise AnsibleError("Error")
