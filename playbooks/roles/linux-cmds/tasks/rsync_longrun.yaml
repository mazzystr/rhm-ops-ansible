---
- name: Checking status of rsync_inprogress_lockfile
  stat:
    path: '/tmp/rsync_inprogress_lockfile'
  register: lockfile

- name: Slurping content of rsync_inprogress_lockfile
  slurp:
      src: '/tmp/rsync_inprogress_lockfile'
  register: status_id
  when: lockfile.stat.exists

- name: Set Ansible status_id
  set_fact:
      status_id: "{{ status_id['content'] | 
        b64decode | 
        replace('# BEGIN ANSIBLE MANAGED BLOCK\n', '') | 
        replace('# END ANSIBLE MANAGED BLOCK\n', '') | 
        replace('\n', '') }}"
  when: lockfile.stat.exists

- name: Checking status of Ansible asyncfile
  stat:
    path: "/root/.ansible_async/{{ status_id }}"
  register: asyncfile
  when: lockfile.stat.exists

- name: Slurping Ansible asyncfile
  slurp:
      src: "/root/.ansible_async/{{ status_id }}"
  register: asyncinfo
  when:
    - lockfile.stat.exists == true
    - asyncfile.skipped is not defined

- name: 'Set fact: asyncinfo'
  set_fact:
      asyncinfo: "{{ asyncinfo['content'] | 
        b64decode | 
        replace('# BEGIN ANSIBLE MANAGED BLOCK\n', '') | 
        replace('# END ANSIBLE MANAGED BLOCK\n', '') | 
        replace('\n', '') }}"
  when:
    - asyncinfo.content is defined

- name: rsyncing data
  ignore_errors: True
  delegate_to: "{{ groups['linux_targets'][0] }}"
  shell: rsync
         -av
         "{{ lcrsync_item.src }}" "{{ lcrsync_item.dest }}"
  args:
    executable: /bin/bash
  async: 1728000 # 20 days!!!
  poll: 0
  register: rsync
  when: lockfile.stat.exists == false or asyncinfo.end is defined

- debug:
    msg:
      - '*********************************************************************'
      - 'rsync failed to start on target'
      - '*********************************************************************'
      - 'Notes:'
      - 'Please rerun this play to attempt to restart rsync'
  when: rsync is not defined
  failed_when: rsync is not defined

- name: Set Ansible status_id
  set_fact:
      status_id: "{{ rsync.ansible_job_id }}"
  when: 
    - rsync.ansible_job_id is defined
  
- name: Creating rsync_inprogress_lockfile
  blockinfile:
    create: yes
    path: /tmp/rsync_inprogress_lockfile
    state: present
    block: |
      {{ status_id }}
  when: rsync|succeeded

- debug:
    msg:
      - '*********************************************************************'
      - 'rsync is currently running on target'
      - '*********************************************************************'
      - 'Notes:'
      - '1) You may kill this ansible job now and come back later.  Rsync will'
      - 'continue to run in the background until the job completes.'
      - '2) First run: Expect rsync task to take day(s) depending on EC2 size and'
      - 'amount of data to transfer.'
      - '3) Subsequent runs: Expect rsync task to take about 100s to verify'
      - '500 Gb of data on a moderate sized EC2 or longer if there are deltas'
      - 'that need to be syncd'
      - '4) How to check status of rsync: df -k and iostat -xn are completely'
      - 'unreliable for AWS EFS.  Do not use them to verify transfer.  Ansible'
      - 'also handles rsync status very poorly to not at all.'
      - 'Instead do this...'
      - 'ssh hadmin@<env>-noc-backup1'
      - 'sudo su -'
      - 'find /data -type f | wc -l; echo; find /data1 -type f | wc -l'
      - 'Observe the delta between the wc outputs'
  when: rsync|succeeded

- name: Querying rsync task
  async_status: jid={{ status_id }}
  register: rsync
  until: rsync.finished
  retries: 9999
  when: status_id is defined

- name: Removing rsync lockfile
  file:
    state: absent
    path: /tmp/rsync_inprogress_lockfile

- name: Removing Ansible asyncfile
  file:
    state: absent
    path: /root/.ansible_async/{{ status_id }}
