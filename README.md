# ansible-ops repo

## Path: `./`

## Purpose

This repository stores all ansible based tooling for provisioning/managing RHMAP SaaS infrastructure.

## Structure

The structure of the repo is generally as follows:

```
.
├── README.md
├── ansible.cfg
├── inventory
    ├── README.md
│   └── hosts
├── library
    ├── README.md
└── playbooks
    ├── README.md
    ├── modules
    ├── roles
    │   └── common
    │       ├── README.md
    │       └── tasks
    │           └── main.yaml
    └── vars
        ├── README.md
        └── main.yaml
```

### Ansible config

This is the [configuration file](http://docs.ansible.com/ansible/latest/intro_configuration.html) used by this instance of Ansible.

```
.
├── ansible.cfg
```

### Inventory

The [Inventory](http://docs.ansible.com/ansible/latest/intro_inventory.html) describes the infrastructure that needs to be interacted with. It typically consists of hosts, which can be grouped together into 'host groups'. It can also be [dynamic](http://docs.ansible.com/ansible/latest/intro_dynamic_inventory.html). More information can be in the README of this directory.

```
.
├── inventory <--
├── library
└── playbooks
```

### Library

Within the team, there should be the goal of reuse as much as possible. The idea is, that if there is no module available to service the requirement, then it is valid to create a custom [module](http://docs.ansible.com/ansible/latest/dev_guide/developing_modules.html). This directory is a library for the custom modules. More information can be in the README of this directory.

```
.
├── inventory
├── library <--
└── playbooks
```

### Playbooks

This directory contains the playbooks and roles that actually do the work. More information can be in the README of this directory.

```
.
├── inventory
├── library
└── playbooks <--
```

#### Execute a playbook

First install virtualenv

```
sudo pip install virtualenv
```

Create a python virtual env called ~/virtualenvs/ansible

```
cd ~/virtualenvs
virtualenv ansible
cd ansible 
source bin/activate
```

Ensure dependencies are installed

```
if [ "$(uname -s)" == 'Darwin' ]; then
  brew update 
  brew install Caskroom/cask/java
  brew install pkg-config libffi openssl
fi
if [ "$(uname -s)" == 'Linux' ]; then
  sudo yum install java-1.8.0-openjdk
  sudo yum install -y openssl-devel patch jq libsemanage-python gcc kernel-devel kernel-headers dkms make bzip2 redhat-rpm-config python-devel libselinux-python redhat-rpm-config
  sudo cp -rp /usr/lib64/python2.7/site-packages/selinux ${VIRTUAL_ENV}/lib/python2.7/site-packages
  sudo cp -rp /usr/lib64/python2.7/site-packages/_selinux.so ${VIRTUAL_ENV}/lib/python2.7/site-packages
fi
pip install --upgrade pip
for i in boto boto3 configparser setuptools paramiko PyYAML Jinja2 httplib2 six pyopenssl netaddr; do pip install $i; done
```

Install Ansible
The version of Ansible which we use is determined by our upstream *openshift_ansible* and at the current date *2017-10-13*, this is determined to be *2.3.1.0*

```
pip install ansible==2.3.1.0
```


Create a symlink to the rhm-devops/rhm-ansible-ops repo

```
cd ~/virtualenvs
ln -s ~/repos/rhm-ansible-ops
```


Add the following to your ~/.bash_profile

```
export VIRTUALENV_HOME='~/virtualenvs/ansible'
```


At this point it is possible to execute a playbook. See the following example:

```
cd ~/virtualenvs/rhm-ansible-ops
ansible-playbook playbooks/generate_rhmap_inventory.yaml
```





